# terraform-external-access-s3

Terraform scripts to build S3 bucket and allow external system access with assume role and externalId. see https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create_for-user_externalid.html