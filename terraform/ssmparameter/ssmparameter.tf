
variable parameter_name {}
variable parameter_type {}
variable parameter_value {}
variable parameter_description {}

resource "aws_ssm_parameter" "ssm_parameter" {
  name  = "${var.parameter_name}"
  type  = "${var.parameter_type}"
  value = "${var.parameter_value}"
  description = "${var.parameter_description}"
}
