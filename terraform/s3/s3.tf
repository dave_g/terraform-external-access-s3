variable bucket_name {}
variable tag_name {}
variable tag_app_name {}
variable tag_env {}
variable description {}
variable sns_topic_arn {}

resource "aws_s3_bucket" "s3_bucket" {
  bucket = "${var.bucket_name}"
  acl    = "private"

  tags = {
    Name        = "${var.tag_name}"
    Environment = "${var.tag_env}"
    Description = "${var.description}"
    ApplicationName = "${var.tag_app_name}"
    ApplicationEnvironment = "${var.tag_env}"
  }

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST", "GET", "HEAD", "DELETE",]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }
}

resource "aws_s3_bucket_public_access_block" "s3_bucket_public_access_block" {
  bucket = "${aws_s3_bucket.s3_bucket.id}"

  block_public_acls   = true
  block_public_policy = true
  ignore_public_acls = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${aws_s3_bucket.s3_bucket.id}"

  topic {
    topic_arn     = "${var.sns_topic_arn}"
    events        = ["s3:ObjectCreated:*", "s3:ObjectRemoved:*"]
  }
}
