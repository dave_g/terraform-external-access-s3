
variable accountid {}
variable bucket_name {}
variable sns_topic_name {}
variable role_name {}
variable tag_name {}
variable tag_app_name {}
variable tag_env {}
variable description {}

resource "aws_iam_role" "iam_role" {
  name = "${var.role_name}"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "AWS": [
            "arn:aws:iam::123456789:root",
            "arn:aws:iam::987654321:root" ]
        },
        "Action": "sts:AssumeRole",
        "Condition": {
          "StringEquals": {
            "sts:ExternalId": "GUID-TO-EXTERAL-SYSTEM"
           }
         }
       }
     ]
  }
EOF

  tags = {
      Name        = "${var.tag_name}"
      Environment = "${var.tag_env}"
      Description = "${var.description}"
      ApplicationName = "${var.tag_app_name}"
      ApplicationEnvironment = "${var.tag_env}"
  }
}

resource "aws_iam_policy" "policy" {
  name        = "external-access-policy"
  description = "policy"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "iam:SimulatePrincipalPolicy",
          "s3:HeadBucket"
        ],
        "Resource": "*"
      },
      {
        "Effect": "Allow",
        "Action": [
          "s3:DeleteObjectTagging",
          "s3:ListBucketMultipartUploads",
          "s3:DeleteObjectVersion",
          "s3:ListBucketVersions",
          "s3:RestoreObject",
          "s3:ListBucket",
          "s3:GetBucketNotification",
          "s3:ListMultipartUploadParts",
          "s3:PutObject",
          "s3:GetObjectAcl",
          "s3:GetObject",
          "s3:AbortMultipartUpload",
          "s3:PutObjectVersionAcl",
          "s3:GetObjectVersionAcl",
          "s3:GetObjectTagging",
          "s3:PutObjectTagging",
          "s3:DeleteObject",
          "s3:GetBucketLocation",
          "s3:PutObjectAcl",
          "s3:GetObjectVersion"
        ],
        "Resource": [
          "arn:aws:s3:::${var.bucket_name}",
          "arn:aws:s3:::${var.bucket_name}/*"
        ]
      },
      {
        "Effect": "Allow",
        "Action": [
          "SNS:ListSubscriptionsByTopic",
          "SNS:Subscribe"
        ],
        "Resource": [
          "arn:aws:s3:::${var.bucket_name}/*",
          "arn:aws:s3:::${var.bucket_name}",
          "arn:aws:sns:us-west-2:${var.accountid}:${var.sns_topic_name}"
        ]
      },
    {
      "Sid": "SDVIParameterStoreAccess",
      "Effect": "Allow",
      "Action": [
        "ssm:DescribeParameters",
        "ssm:GetParameters",
        "ssm:GetParameter",
        "ssm:PutParameter"
      ],
      "Resource": [
        "arn:aws:ssm:us-west-2:${var.accountid}:parameter/external*"
      ]
    }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "role-policy-attachment" {
    role = "${aws_iam_role.iam_role.name}"
    //policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess"
    policy_arn = "${aws_iam_policy.policy.arn}"
}
