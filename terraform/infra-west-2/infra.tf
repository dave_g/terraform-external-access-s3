locals { 
  region     = "us-west-2" //todo : change to correct region
  // todo : re-evaluate tag passing after 0.12 terraform is released that will Support count in resource fields #7034 - https://github.com/hashicorp/terraform/issues/7034
  tag_app_name = "External-S3-Access"
  tag_env = "development"
  bucket_name = "external-access-bucket"
  accountid = "123456789" 
  role_name = "external-access-role"
}

provider "aws" {
  region = "${local.region}"
  profile = "ttdAdmin"
  allowed_account_ids = ["${local.accountid}"]
  shared_credentials_file = "~/.aws/credentials"
}

module "sns" {
  source = "../s3-sns"
  tag_app_name = "${local.tag_app_name}"
  tag_env = "${local.tag_env}"
  tag_name = "external sns"
  bucket_name = "${local.bucket_name}"
  description = "sns for bucket events that external processes will read."
  sns_name = "external-notify-${local.bucket_name}"
  accountid = "${local.accountid}"
  region = "${local.region}"
}

module "s3" {
  source = "../s3"
  tag_app_name = "${local.tag_app_name}"
  tag_env = "${local.tag_env}"
  tag_name = "external main test"
  bucket_name = "${local.bucket_name}"
  description = "test bucket for external processes."
  sns_topic_arn = "${module.sns.arn}"
}

module "externalrole" {
  source = "../externalrole"
  tag_app_name = "${local.tag_app_name}"
  tag_env = "${local.tag_env}"
  tag_name = "external custom role"
  bucket_name = "${local.bucket_name}"
  description = "role that external processes will assume."
  sns_topic_name = "external-notify-${local.bucket_name}"
  accountid = "${local.accountid}"
  role_name = "${local.role_name}"
}

module "ssmparameter" {
  source = "../ssmparameter"
  parameter_name = "externalSFTPCred"
  parameter_type = "String"
  parameter_description = "external sftp parameters"
  parameter_value = <<EOF
  {"user": "external_sftp", "password": "needToSetManualy"}
EOF
}
