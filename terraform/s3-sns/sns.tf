variable bucket_name {}
variable sns_name {}
variable accountid {}
variable region {}

variable tag_name {}
variable tag_app_name {}
variable tag_env {}
variable description {}


resource "aws_sns_topic" "sns_topic" {
  name = "${var.sns_name}"

  tags = {
    Name        = "${var.tag_name}"
    Environment = "${var.tag_env}"
    Description = "${var.description}"
    "ApplicationName" = "${var.tag_app_name}"
    "ApplicationEnvironment" = "${var.tag_env}"
  }

  policy = <<EOF
  {
    "Version": "2008-10-17",
    "Id": "__default_policy_ID",
    "Statement": [
      {
        "Sid": "__default_statement_ID",
        "Effect": "Allow",
        "Principal": {
          "AWS": "*"
        },
        "Action": [
          "SNS:GetTopicAttributes",
          "SNS:SetTopicAttributes",
          "SNS:AddPermission",
          "SNS:RemovePermission",
          "SNS:DeleteTopic",
          "SNS:Subscribe",
          "SNS:ListSubscriptionsByTopic",
          "SNS:Publish",
          "SNS:Receive"
        ],
        "Resource": "arn:aws:sns:${var.region}:${var.accountid}:${var.sns_name}",
        "Condition": {
          "StringEquals": {
            "AWS:SourceOwner": "${var.accountid}"
          }
        }
      },
      {
        "Sid": "sdviStmt0001",
        "Effect": "Allow",
        "Principal": {
          "AWS": "*"
        },
        "Action": "SNS:Publish",
        "Resource": "arn:aws:sns:${var.region}:${var.accountid}:${var.sns_name}",
        "Condition": {
          "ArnLike": {
            "aws:SourceArn": "arn:aws:s3:::${var.bucket_name}"
          }
        }
      }
    ]
  }
EOF
}

output "arn" {
  value = "${aws_sns_topic.sns_topic.arn}"
}

output "id" {
  value = "${aws_sns_topic.sns_topic.id}"
}
